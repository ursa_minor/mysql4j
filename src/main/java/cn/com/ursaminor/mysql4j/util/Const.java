package cn.com.ursaminor.mysql4j.util;

/**
 * 该类定义若干常数.
 * 
 * @author 小熊
 * @version 1.0
 */
public class Const
{
	public static final String LEFT_JOIN 	= " left join ";
	public static final String RIGHT_JOIN 	= " right join ";
	public static final String INNER_JOIN 	= " inner join ";
	public static final String OUTER_JOIN 	= " outer join ";
	public static final String JOIN 		= " join ";
	public static final String CROSS_JOIN 	= " cross join ";
	public static final String ON 	= " on ";
}
