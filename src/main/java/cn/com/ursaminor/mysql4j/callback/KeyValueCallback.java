package cn.com.ursaminor.mysql4j.callback;

import cn.com.ursaminor.mysql4j.core.Query;

/**
 * 该接口用于{@link Query}类中，用来给出以Map形式表示的数据结果集在Map对象中每条记录对应的key值和value值.
 * 
 * @param <K> 返回的key的类型，通常为String、Integer、Long
 * @param <V> 返回的value的类型
 * @param <R> 数据装载类类型，通常为pojo类型，Row类型
 * @author 小熊
 * @version 1.0
 */
public interface KeyValueCallback<K,V,R>
{
	/**
	 *	该方法从数据对象中生成一个key值，返回给调用者.
	 *
	 *  <p>
	 *  {@link Query}的getList方法返回的结果集，是以List表示的
	 *  数据对象列表。有时，我们需要一个Map，此时调用getMap方法即可。
	 *  Map的key值是该行数据的某个字段或者某几个字段组合，value值是该行数据对象本身，即原来
	 *  List列表中的元素。本方法即为给出key值需要实现的方法。
	 *  </p>
	 *  <p>
	 *  Map的形式如下：
	 *  <pre>
	 *  {
	 *      key1 : value1,
	 *      key2 : value2,
	 *      ...
	 *      keyn : valuen
	 *  }
	 *  </pre>
	 *
	 *	@param row 数据对象，可为pojo或者Row
	 *  @return key值
	 */
	public K getKey(R row);
	
	/**
	 *	该方法从数据对象中生成一个value值，返回给调用者.
	 *
	 *  <p>
	 *  {@link Query}的getList方法返回的结果集，是以List表示的
	 *  数据对象列表。有时，我们需要一个Map，此时调用getMap方法即可。
	 *  Map的key值由{@link #getKey(Object)}方法给出，value由{@link #getValue(Object)}给出。
	 *  </p>
	 *  <p>
	 *  Map的形式如下：
	 *  <pre>
	 *  {
	 *      key1 : value1,
	 *      key2 : value2,
	 *      ...
	 *      keyn : valuen
	 *  }
	 *  </pre>
	 *
	 *	@param row 数据对象，可为pojo或者Row
	 *  @return value值
	 */
	public V getValue(R row);
}
