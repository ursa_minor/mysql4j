package cn.com.ursaminor.mysql4j.core;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * 该类定义一个抽象表，有两个子类：{@link Table}和{@link Query}，分别用来表示实体表和查询语句
 * 产生的虚拟表.
 * 
 * <p>该类管理{@code JdbcTemplate}属性和别名.</p>
 * <p>JdbcTemplate用来访问mysql数据库，需要保证可用.</p>
 * <p>当该类的构造方法被创建时，会自动分配一个别名，此别名不可更改，将作为sql中表的别名使用.</p>
 * 
 * @author 小熊
 * @version 1.0
 * @see Table
 * @see Query
 */
public abstract class AbstTable implements StringChip
{
	private static int index = -1;
	
	protected JdbcTemplate jdbcTemplate;	// JdbcTemplate
	private String alias;					// 别名
	
	
	// 无参数构造方法，由子类调用，自动分配一个别名
	AbstTable()
	{
		if(++index>=10000) index = 0;
		alias = "t"+index;
	}
	
	//获得全称，该名称在Table类中，形式为 “表名 as 表别名”，在Query类中，形式为 “(sql语句) as 表别名”
	public abstract String getString();

	/**
	 *	获得别名
	 *	@return 别名
	 */
	public String getAlias()
	{
		return alias;
	}
	
	/**
	 *	获得JdbcTemplate对象
	 *	@return JdbcTemplate对象，如果未配置，则为null
	 */
	public JdbcTemplate getJdbcTemplate()
	{
		return jdbcTemplate;
	}

	/**
	 *	设置JdbcTemplate对象
	 *	@param jdbcTemplate JdbcTemplate对象
	 */
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
	{
		this.jdbcTemplate = jdbcTemplate;
	}
}