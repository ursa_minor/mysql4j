package cn.com.ursaminor.mysql4j.core;

/**
 * 该接口表示一个字符串片段.
 * 
 * @author 小熊
 * @version 1.0
 */
public interface StringChip
{
	public String getString();
}
