package cn.com.ursaminor.mysql4j.core;

import java.lang.reflect.Method;

import cn.com.ursaminor.mysql4j.util.Mysql4jUtil;

/**
 * 该类定义一个字段.
 * 
 * <p>类{@link Chr}、{@link Num}、{@link Datetime}继承了此类，用来标识不同的数据类型.</p>
 * <p>该类记录字段所属的表、主键属性、自增长属性、字段名、pojo类中的对应属性以及该pojo属性的getter方法</p>
 * 
 * @author 小熊
 * @version 1.0
 * @see Chr
 * @see Num
 * @see Datetime
 */
public class Column implements StringChip
{
	protected AbstTable table;					// 表
	protected boolean pk;						// 是否主键
	protected boolean increment;				// 是否自增长
	
	protected String name;						// 字段名
	protected String property;					// pojo类属性
	protected Method method;					// pojo类get方法
	
	/**
	 *	获得当前表（配置类）.
	 *
	 *  @return 配置类
	 */
	public AbstTable getTable()
	{
		return table;
	}
	
	/**
	 *	获得字段名.
	 *
	 *  @return 字段名
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 *	获得pojo类对应属性名.
	 *
	 *  @return 属性名
	 */
	public String getProperty()
	{
		return property;
	}

	/**
	 *	获得pojo类对应属性getter方法对象.
	 *
	 *  @return Method对象
	 */
	public Method getMethod()
	{
		return method;
	}

	/**
	 *	是否主键.
	 *
	 *  @return 是否
	 */
	public boolean isPrimaryKey()
	{
		return pk;
	}

	/**
	 *	是否自增长.
	 *
	 *  @return 是否
	 */
	public boolean isIncrement()
	{
		return increment;
	}

	/**
	 *	设置别名，该方法仅在子查询中使用.
	 *
	 *	@param column 用该参数的name属性作为别名
	 *  @return 当前对象
	 */
	public Column as(Column column)
	{
		return new ColumnAlias(this,column.name);
	}
	
	/**
	 *	倒序排序.
	 *
	 *  <p>
	 *  该方法在Query类的order(FunExpr expr)方法中使用，如{@code order(m.username.desc())}
	 *  </p>
	 *
	 *  @return FunExpr对象
	 */
	public FunExpr desc()
	{
		return Fun.eval(getString()+" desc");
	}
	
	/**
	 *	正序排序.
	 *
	 *  <p>
	 *  该方法在Query类的order(FunExpr expr)方法中使用，如{@code order(m.username.asc())}
	 *  </p>
	 *
	 *  @return FunExpr对象
	 */
	public FunExpr asc()
	{
		return Fun.eval(getString()+" asc");
	}
	
	// 表别名.列名:  t1.name
	String getAliasWithName()
	{
		if(Mysql4jUtil.isEmpty(table.getAlias())) return name;
		else return table.getAlias() + "." + name;
	}
	
	// 列名 as 属性名：name as username
	String getNameWithProperty()
	{
		if(Mysql4jUtil.isEmpty(property)) return name;
		else return name + " as " + property;
	}
	
	// 列名 as 列别名：name as n
	String getNameWithAlias()
	{
		//if(Mysql4jUtil.isEmpty(alias)) return name;
		//else return name + " as " + alias;
		return name;
	}
	
	// 表别名.列名 as 属性名：t1.name as username
	String getAliasWithNameWithProperty()
	{
		if(Mysql4jUtil.isEmpty(property)) return getAliasWithName();
		else return getAliasWithName() + " as " + property;
	}
	
	// 表别名.列名 as 列别名：t1.name as n   //有使用
	String getAliasWithNameWithAlias()
	{
		//if(Mysql4jUtil.isEmpty(alias)) return getAliasWithName();
		//else return getAliasWithName() + " as " + alias;
		return getAliasWithName();
	}
	
	String getAliasOrName()
	{
		//if(Mysql4jUtil.notEmpty(alias)) return alias;
		//else return name;
		return name;
	}
	
	String getPropertyOrName()
	{
		if(Mysql4jUtil.notEmpty(property)) return property;
		else return name;
	}
	
	String getAliasOrPropertyOrName()
	{
		//if(Mysql4jUtil.notEmpty(alias)) return alias;
		//else if(Mysql4jUtil.notEmpty(property)) return property;
		//else return name;
		if(Mysql4jUtil.notEmpty(property)) return property;
		else return name;
	}
	
	/**
	 *	字符串表示，返回“表别名.列名”.
	 *
	 *  @return 字符串
	 */
	public String getString()
	{
		return getAliasWithName();
	}
}