package cn.com.ursaminor.mysql4j.core;

/**
 * 该类定义mysql内置函数.
 *  
 * @author 小熊
 * @version 1.0
 */
public class Fun
{
	/**
	 *	把字符串字面量作为sql函数.
	 *
	 *	@param s 函数表达式
	 *  @return FunExpr对象
	 */
	public static FunExpr eval(String s)
	{
		return new FunExpr(s);
	}

	/**
	 *	distinct操作符.
	 *
	 *	@param c 被操作表达式
	 *  @return FunExpr对象
	 */
	public static FunExpr distinct(StringChip c)
	{
		return new FunExpr("distinct "+c.getString());
	}

	/**
	 *	distinct操作符.
	 *
	 *	@param c 被操作表达式
	 *  @return FunExpr对象
	 */
	public static FunExpr distinct(String c)
	{
		return new FunExpr("distinct "+c);
	}

	/**
	 *	count函数.
	 *
	 *	@param c 函数的参数
	 *  @return FunExpr对象
	 */
	public static FunExpr count(StringChip c)
	{
		return new FunExpr("count("+c.getString()+")");
	}

	/**
	 *	count函数.
	 *
	 *	@param c 函数的参数
	 *  @return FunExpr对象
	 */
	public static FunExpr count(String c)
	{
		return new FunExpr("count("+c+")");
	}

	/**
	 *	sum函数.
	 *
	 *	@param c 函数的参数
	 *  @return FunExpr对象
	 */
	public static FunExpr sum(StringChip c)
	{
		return new FunExpr("sum("+c.getString()+")");
	}

	/**
	 *	sum函数.
	 *
	 *	@param c 函数的参数
	 *  @return FunExpr对象
	 */
	public static String sum(String c)
	{
		return "sum("+c+")";
	}

	/**
	 *	min函数.
	 *
	 *	@param c 函数的参数
	 *  @return FunExpr对象
	 */
	public static FunExpr min(StringChip c)
	{
		return new FunExpr("min("+c.getString()+")");
	}

	/**
	 *	min函数.
	 *
	 *	@param c 函数的参数
	 *  @return FunExpr对象
	 */
	public static String min(String c)
	{
		return "min("+c+")";
	}

	/**
	 *	max函数.
	 *
	 *	@param c 函数的参数
	 *  @return FunExpr对象
	 */
	public static FunExpr max(StringChip c)
	{
		return new FunExpr("max("+c.getString()+")");
	}

	/**
	 *	max函数.
	 *
	 *	@param c 函数的参数
	 *  @return FunExpr对象
	 */
	public static String max(String c)
	{
		return "max("+c+")";
	}

	/**
	 *	avg函数.
	 *
	 *	@param c 函数的参数
	 *  @return FunExpr对象
	 */
	public static FunExpr avg(StringChip c)
	{
		return new FunExpr("avg("+c.getString()+")");
	}

	/**
	 *	avg函数.
	 *
	 *	@param c 函数的参数
	 *  @return FunExpr对象
	 */
	public static String avg(String c)
	{
		return "avg("+c+")";
	}

	/**
	 *	CONCAT_WS函数.
	 *
	 *	@param separator 分隔符
	 *	@param values 待连接的值
	 *  @return FunExpr对象
	 */
	public static FunExpr concatWs(String separator, StringChip...values)
	{
		if(separator==null) separator = "";
		String expr = "CONCAT_WS('"+separator+"'";
		for(StringChip v : values)
		{
			expr += ","+v.getString();
		}
		expr += ")";
		return new FunExpr(expr);
	}
	
	/**
	 *	RAND函数.
	 *
	 *  @return FunExpr对象
	 */
	public static FunExpr rand()
	{
		return new FunExpr("Rand()");
	}
}