package cn.com.ursaminor.mysql4j.core;

import cn.com.ursaminor.mysql4j.util.Mysql4jUtil;

/**
 * 该类表示一个带有别名的字段.
 * 
 * @author 小熊
 * @version 1.0
 * @see Column
 */
public class ColumnAlias extends Column
{
	protected String alias;						// 别名
	
	ColumnAlias(Column column, String alias)
	{
		super.table = column.table;
		super.pk = column.pk;
		super.increment = column.increment;
		super.name = column.name;
		super.property = column.property;
		super.method = column.method;
		this.alias = alias;
	}
	
	// 表别名.列名:  t1.name
	String getAliasWithName()
	{
		if(Mysql4jUtil.isEmpty(table.getAlias())) return name;
		else return table.getAlias() + "." + name;
	}
	
	// 列名 as 属性名：name as username
	String getNameWithProperty()
	{
		if(Mysql4jUtil.isEmpty(property)) return name;
		else return name + " as " + property;
	}
	
	// 列名 as 列别名：name as n
	String getNameWithAlias()
	{
		if(Mysql4jUtil.isEmpty(alias)) return name;
		else return name + " as " + alias;
	}
	
	// 表别名.列名 as 属性名：t1.name as username
	String getAliasWithNameWithProperty()
	{
		if(Mysql4jUtil.isEmpty(property)) return getAliasWithName();
		else return getAliasWithName() + " as " + property;
	}
	
	// 表别名.列名 as 列别名：t1.name as n   //有使用
	String getAliasWithNameWithAlias()
	{
		if(Mysql4jUtil.isEmpty(alias)) return getAliasWithName();
		else return getAliasWithName() + " as " + alias;
	}
	
	// 取出别名或者字段名，别名优先
	String getAliasOrName()
	{
		if(Mysql4jUtil.notEmpty(alias)) return alias;
		else return name;
	}
	
	// 取出属性名或者字段名，属性名优先
	String getPropertyOrName()
	{
		if(Mysql4jUtil.notEmpty(property)) return property;
		else return name;
	}
	
	// 取出别名、属性名或者字段名，别名、属性名优先
	String getAliasOrPropertyOrName()
	{
		if(Mysql4jUtil.notEmpty(alias)) return alias;
		else if(Mysql4jUtil.notEmpty(property)) return property;
		else return name;
	}
	
	public String getString()
	{
		return getAliasWithName();
	}
}