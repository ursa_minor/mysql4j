package cn.com.ursaminor.mysql4j.core;

/**
 * 该类定义一个函数表达式.
 *  
 * @author 小熊
 * @version 1.0
 * @see BoolExpr
 */
public class FunExpr implements StringChip
{
	private String exprStr = null;
	private String alias;
	
	FunExpr(String exp)
	{
		exprStr = exp;
	}

	/**
	 *	设置别名.
	 *
	 *	@param alias 别名
	 *  @return FunExpr对象
	 */
	public FunExpr as(String alias)
	{
		this.alias = alias;
		return this;
	}

	/**
	 *	设置别名.
	 *
	 *	@param column 别名
	 *  @return FunExpr对象
	 */
	public FunExpr as(Column column)
	{
		this.alias = column.name;
		return this;
	}

	/**
	 *	获得别名.
	 *
	 *  @return 别名
	 */
	public String getAlias()
	{
		return alias;
	}

	/**
	 *	对当前表达式结果进行“等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr eq(StringChip value)
	{
		boolean isnull = value==null || value.getString()==null || value.getString().length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getString()+"="+value.getString(), isnull);
	}

	/**
	 *	对当前表达式结果进行“等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr eq(Number value)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr(this.getString()+" is null", isnull);
		else return new BoolExpr(this.getString()+"="+value.toString(), isnull);
	}

	/**
	 *	对当前表达式结果进行“不等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr notEq(StringChip value)
	{
		boolean isnull = value==null || value.getString()==null || value.getString().length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getString()+"<>"+value.getString(), isnull);
	}

	/**
	 *	对当前表达式结果进行“不等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr notEq(Number value)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr(this.getString()+" is not null", isnull);
		else return new BoolExpr(this.getString()+"<>"+value.toString(), isnull);
	}

	/**
	 *	对当前表达式结果进行“大于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr gt(StringChip value)
	{
		boolean isnull = value==null || value.getString()==null || value.getString().length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getString()+">"+value.getString(), isnull);
	}

	/**
	 *	对当前表达式结果进行“大于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr gt(Number value)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getString()+">"+value.toString(), isnull);
	}

	/**
	 *	对当前表达式结果进行“大于等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr ge(StringChip value)
	{
		boolean isnull = value==null || value.getString()==null || value.getString().length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getString()+">="+value.getString(), isnull);
	}

	/**
	 *	对当前表达式结果进行“大于等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr ge(Number value)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getString()+">="+value.toString(), isnull);
	}

	/**
	 *	对当前表达式结果进行“小于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr lt(StringChip value)
	{
		boolean isnull = value==null || value.getString()==null || value.getString().length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getString()+"<"+value.getString(), isnull);
	}

	/**
	 *	对当前表达式结果进行“小于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr lt(Number value)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getString()+"<"+value.toString(), isnull);
	}

	/**
	 *	对当前表达式结果进行“小于等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr le(StringChip value)
	{
		boolean isnull = value==null || value.getString()==null || value.getString().length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getString()+"<="+value.getString(), isnull);
	}

	/**
	 *	对当前表达式结果进行“小于等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr le(Number value)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getString()+"<="+value.toString(), isnull);
	}

	public String getString()
	{
		if(exprStr==null || exprStr.length()==0) return null;
		if(alias==null || alias.length()==0) return exprStr;
		else return exprStr+" as "+alias;
	}
}
