package cn.com.ursaminor.mysql4j.core;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import cn.com.ursaminor.mysql4j.util.Mysql4jUtil;

/**
 * 该类实现了Map接口，增加了若干提取数据的便利方法，可以作为通用数据类使用.
 * 
 * @author 小熊
 * @version 1.0
 */
public class Row implements Map
{
	private Map map;
	
	/**
	 *	构造方法.
	 *
	 *  @param map 数据对象
	 */
	public Row(Map map)
	{
		this.map = map;
	}
	
	/**
	 *	取出数据Map对象，该对象由JdbcTemplate提供.
	 *
	 *  @return Map对象
	 */
	public Map getMap()
	{
		return map;
	}
	
	/**
	 *	取出字符串.
	 *
	 *	@param column 该字段的name属性作为key值
	 *  @return value值
	 */
	public String getString(Column column)
	{
		Object value =  map.get(column.getAliasOrName());
		return value==null?null:value.toString();
	}

	/**
	 *	取出字符串.
	 *
	 *	@param key 键值
	 *  @return value值
	 */
	public String getString(String key)
	{
		Object value =  map.get(key);
		return value==null?null:value.toString();
	}

	/**
	 *	取出Integer类型整数.
	 *
	 *	@param column 该字段的name属性作为key值
	 *  @return value值
	 */
	public Integer getInteger(Column column)
	{
		Object value =  map.get(column.getAliasOrName());
		return Mysql4jUtil.getInteger(value, null);
	}

	/**
	 *	取出Integer类型整数.
	 *
	 *	@param key 键值
	 *  @return value值
	 */
	public Integer getInteger(String key)
	{
		Object value =  map.get(key);
		return Mysql4jUtil.getInteger(value, null);
	}

	/**
	 *	取出Long类型整数.
	 *
	 *	@param column 该字段的name属性作为key值
	 *  @return value值
	 */
	public Long getLong(Column column)
	{
		Object value =  map.get(column.getAliasOrName());
		return Mysql4jUtil.getLong(value, null);
	}

	/**
	 *	取出Long类型整数.
	 *
	 *	@param key 键值
	 *  @return value值
	 */
	public Long getLong(String key)
	{
		Object value =  map.get(key);
		return Mysql4jUtil.getLong(value, null);
	}

	/**
	 *	取出Float类型浮点数.
	 *
	 *	@param column 该字段的name属性作为key值
	 *  @return value值
	 */
	public Float getFloat(Column column)
	{
		Object value =  map.get(column.getAliasOrName());
		return Mysql4jUtil.getFloat(value, null);
	}

	/**
	 *	取出Float类型浮点数.
	 *
	 *	@param key 键值
	 *  @return value值
	 */
	public Float getFloat(String key)
	{
		Object value =  map.get(key);
		return Mysql4jUtil.getFloat(value, null);
	}

	/**
	 *	取出Double类型浮点数.
	 *
	 *	@param column 该字段的name属性作为key值
	 *  @return value值
	 */
	public Double getDouble(Column column)
	{
		Object value =  map.get(column.getAliasOrName());
		return Mysql4jUtil.getDouble(value, null);
	}

	/**
	 *	取出Double类型浮点数.
	 *
	 *	@param key 键值
	 *  @return value值
	 */
	public Double getDouble(String key)
	{
		Object value =  map.get(key);
		return Mysql4jUtil.getDouble(value, null);
	}

	/**
	 *	取出Boolean类型值.
	 *
	 *	@param column 该字段的name属性作为key值
	 *  @return value值
	 */
	public Boolean getBoolean(Column column)
	{
		Object value =  map.get(column.getAliasOrName());
		return Mysql4jUtil.getBoolean(value, null);
	}

	/**
	 *	取出Boolean类型值.
	 *
	 *	@param key 键值
	 *  @return value值
	 */
	public Boolean getBoolean(String key)
	{
		Object value =  map.get(key);
		return Mysql4jUtil.getBoolean(value, null);
	}

	/**
	 *	取出Date对象.
	 *
	 *	@param column 该字段的name属性作为key值
	 *  @return value值
	 */
	public Date getDate(Column column)
	{
		return getDate(column.getAliasOrName());
	}

	/**
	 *	取出Date对象.
	 *
	 *	@param key 键值
	 *  @return value值
	 */
	public Date getDate(String key)
	{
		Object value =  map.get(key);
		if(value==null) return null;
		else if(value instanceof Date) return (Date)value;
		else if(value instanceof String) return Mysql4jUtil.parseDate((String)value);
		else if(value instanceof Integer) return new Date(((Integer)value)*1000L);
		else if(value instanceof Long) return new Date((Long)value);
		return null;
	}

	// ------------------------------------------------------------------------------
	
	@Override
	public int size()
	{
		return map.size();
	}

	@Override
	public boolean isEmpty()
	{
		return map.isEmpty();
	}

	@Override
	public boolean containsKey(Object key)
	{
		return map.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value)
	{
		return map.containsValue(value);
	}

	@Override
	public Object get(Object key)
	{
		return map.get(key);
	}

	@Override
	public Object put(Object key, Object value)
	{
		return map.put(key, value);
	}

	@Override
	public Object remove(Object key)
	{
		return map.remove(key);
	}

	@Override
	public void putAll(Map m)
	{
		map.putAll(m);
	}

	@Override
	public void clear()
	{
		map.clear();
	}

	@Override
	public Set keySet()
	{
		return map.keySet();
	}

	@Override
	public Collection values()
	{
		return map.values();
	}

	@Override
	public Set entrySet()
	{
		return map.entrySet();
	}
}
