package cn.com.ursaminor.mysql4j.core;

import java.util.Collection;

/**
 * 该类定义一个数字型字段.
 *  
 * @author 小熊
 * @version 1.0
 * @see Column
 * @see Chr
 * @see Datetime
 */
public class Num extends Column
{
	/**
	 *	设置pojo类对应的属性.
	 *
	 *	@param property pojo类对应的属性
	 *  @return 当前对象
	 */
	public Num property(String property)
	{
		this.property = property;
		return this;
	}

	/**
	 *	当前字段作为主键.
	 *
	 *  @return 当前对象
	 */
	public Num asPrimaryKey()
	{
		this.pk = true;
		return this;
	}

	/**
	 *	当前字段为自增长.
	 *
	 *  @return 当前对象
	 */
	public Num asIncrement()
	{
		this.increment = true;
		return this;
	}

	/**
	 *	对当前字段进行“等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr eq(StringChip value)
	{
		boolean isnull = value==null || value.getString()==null || value.getString().length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+"="+value.getString(), isnull);
	}

	/**
	 *	对当前字段进行“等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr eq(Number value)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr(this.getAliasWithName()+" is null", isnull);
		else return new BoolExpr(this.getAliasWithName()+"="+value.toString(), isnull);
	}

	/**
	 *	对当前字段进行“不等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr notEq(StringChip value)
	{
		boolean isnull = value==null || value.getString()==null || value.getString().length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		return new BoolExpr(this.getAliasWithName()+"<>"+value.getString(), isnull);
	}

	/**
	 *	对当前字段进行“不等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr notEq(Number value)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr(this.getAliasWithName()+" is not null", isnull);
		else return new BoolExpr(this.getAliasWithName()+"<>"+value.toString(), isnull);
	}

	/**
	 *	对当前字段进行“大于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr gt(StringChip value)
	{
		boolean isnull = value==null || value.getString()==null || value.getString().length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+">"+value.getString(), isnull);
	}

	/**
	 *	对当前字段进行“大于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr gt(Number value)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+">"+value.toString(), isnull);
	}

	/**
	 *	对当前字段进行“大于等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr ge(StringChip value)
	{
		boolean isnull = value==null || value.getString()==null || value.getString().length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+">="+value.getString(), isnull);
	}

	/**
	 *	对当前字段进行“大于等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr ge(Number value)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+">="+value.toString(), isnull);
	}

	/**
	 *	对当前字段进行“小于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr lt(StringChip value)
	{
		boolean isnull = value==null || value.getString()==null || value.getString().length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+"<"+value.getString(), isnull);
	}

	/**
	 *	对当前字段进行“小于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr lt(Number value)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+"<"+value.toString(), isnull);
	}

	/**
	 *	对当前字段进行“小于等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr le(StringChip value)
	{
		boolean isnull = value==null || value.getString()==null || value.getString().length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+"<="+value.getString(), isnull);
	}

	/**
	 *	对当前字段进行“小于等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr le(Number value)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+"<="+value.toString(), isnull);
	}

	/**
	 *	对当前字段进行“in”判断.
	 *
	 *	@param values 被匹配的值数组
	 *  @return BoolExpr对象
	 */
	public BoolExpr in(Number... values)
	{
		String strArray = getString(values);
		boolean isnull = strArray==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" in ("+strArray+")", isnull);
	}

	/**
	 *	对当前字段进行“in”判断.
	 *
	 *	@param values 被匹配的值集合
	 *  @return BoolExpr对象
	 */
	public BoolExpr in(Collection<? extends Number> values)
	{
		String strArray = getString(values);
		boolean isnull = strArray==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" in ("+strArray+")", isnull);
	}

	/**
	 *	对当前字段进行“in”判断.
	 *
	 *	@param value 被匹配的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr in(StringChip value)
	{
		boolean isnull = value==null || value.getString()==null || value.getString().length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" in ("+value.getString()+")", isnull);
	}

	/**
	 *	对当前字段进行“in”判断.
	 *
	 *	@param query 被匹配的子查询
	 *  @return BoolExpr对象
	 */
	public BoolExpr in(Query query)
	{
		String sub = query==null?null:query.getSql();
		boolean isnull = query==null || sub==null || sub.length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" in ("+sub+")", isnull);
	}

	/**
	 *	对当前字段进行“not in”判断.
	 *
	 *	@param values 被匹配的值数组
	 *  @return BoolExpr对象
	 */
	public BoolExpr notIn(Number... values)
	{
		String strArray = getString(values);
		boolean isnull = strArray==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" not in ("+strArray+")", isnull);
	}

	/**
	 *	对当前字段进行“not in”判断.
	 *
	 *	@param values 被匹配的值集合
	 *  @return BoolExpr对象
	 */
	public BoolExpr notIn(Collection<? extends Number> values)
	{
		String strArray = getString(values);
		boolean isnull = strArray==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" not in ("+strArray+")", isnull);
	}

	/**
	 *	对当前字段进行“not in”判断.
	 *
	 *	@param value 被匹配的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr notIn(StringChip value)
	{
		boolean isnull = value==null || value.getString()==null || value.getString().length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" not in ("+value.getString()+")", isnull);
	}

	/**
	 *	对当前字段进行“not in”判断.
	 *
	 *	@param query 被匹配的子查询
	 *  @return BoolExpr对象
	 */
	public BoolExpr notIn(Query query)
	{
		String sub = query==null?null:query.getSql();
		boolean isnull = query==null || sub==null || sub.length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" not in ("+sub+")", isnull);
	}

	private String getString(Collection<? extends Number> values)
	{
		if(values==null) return null;
		String s = null;
		for(Number v : values)
		{
			if(v==null) continue;
			if(s==null) s = ""+v;
			else s += ","+v;
		}
		return s;
	}

	private String getString(Number... values)
	{
		if(values==null) return null;
		String s = null;
		for(Number v : values)
		{
			if(v==null) continue;
			if(s==null) s = ""+v;
			else s += ","+v;
		}
		return s;
	}

	/**
	 *	对当前字段进行“is null”判断.
	 *
	 *  @return BoolExpr对象
	 */
	public BoolExpr isNull()
	{
		return new BoolExpr(this.getString()+" is null");
	}

	/**
	 *	对当前字段进行“is not null”判断.
	 *
	 *  @return BoolExpr对象
	 */
	public BoolExpr notNull()
	{
		return new BoolExpr(this.getString()+" is not null");
	}
}