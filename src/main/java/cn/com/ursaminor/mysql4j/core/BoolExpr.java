package cn.com.ursaminor.mysql4j.core;

import static cn.com.ursaminor.mysql4j.util.Mysql4jUtil.isEmpty;

/**
 * 该类定义一个布尔表达式.
 *  
 * @author 小熊
 * @version 1.0
 * @see FunExpr
 */
public class BoolExpr implements StringChip
{
	private String exprStr = null;
	private int count = 0;
	private boolean isnull;
	
	// 构造方法.
	BoolExpr(String exp)
	{
		exprStr = exp;
		if(!isEmpty(exp)) count = 1;
	}
	
	// 构造方法.
	BoolExpr(String exp, boolean isnull)
	{
		this(exp);
		this.isnull = isnull;
	}
	
	boolean isNull()
	{
		return isnull;
	}

	/**
	 *	对当前表达式和参数指定的表达式进行“AND”运算.
	 *
	 *	@param exp 布尔表达式
	 *  @return 当前BoolExpr对象
	 */
	public BoolExpr and(String exp)
	{
		if(isEmpty(exp)) return this;
		if(isEmpty(exprStr)) exprStr = exp;
		else exprStr += " and "+exp;
		count++;
		return this;
	}

	/**
	 *	对当前表达式和参数指定的表达式进行“AND”运算.
	 *
	 *	@param expr 布尔表达式
	 *  @return 当前BoolExpr对象
	 */
	public BoolExpr and(BoolExpr expr)
	{
		String exp = expr.getString();
		if(isEmpty(exp)) return this;
		if(expr.count>1)
		{
			if(isEmpty(exprStr)) exprStr = exp;
			else exprStr += " and ("+exp+")";
		}
		else
		{
			if(isEmpty(exprStr)) exprStr = exp;
			else exprStr += " and "+exp;
		}
		this.count += expr.count;
		return this;
	}

	/**
	 *	对当前表达式和参数指定的表达式进行“OR”运算.
	 *
	 *	@param exp 布尔表达式
	 *  @return 当前BoolExpr对象
	 */
	public BoolExpr or(String exp)
	{
		if(isEmpty(exp)) return this;
		if(isEmpty(exprStr)) exprStr = exp;
		else exprStr += " or "+exp;
		count++;
		return this;
	}

	/**
	 *	对当前表达式和参数指定的表达式进行“OR”运算.
	 *
	 *	@param expr 布尔表达式
	 *  @return 当前BoolExpr对象
	 */
	public BoolExpr or(BoolExpr expr)
	{
		String exp = expr.getString();
		if(isEmpty(exp)) return this;
		if(expr.count>1)
		{
			if(isEmpty(exprStr)) exprStr = exp;
			else exprStr += " or ("+exp+")";
		}
		else
		{
			if(isEmpty(exprStr)) exprStr = exp;
			else exprStr += " or "+exp;
		}
		
		this.count += expr.count;
		return this;
	}
	
	String getExprStr()
	{
		return exprStr;
	}

	int getCount()
	{
		return count;
	}

	/**
	 *	获得表达式字符串.
	 *
	 *  @return 布尔表达式字符串
	 */
	public String getString()
	{
		if(isEmpty(exprStr)) return null;
		else return exprStr;
	}
}
