package cn.com.ursaminor.mysql4j.core;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 该类定义一个datetime类型字段.
 *  
 * @author 小熊
 * @version 1.0
 * @see Column
 * @see Chr
 * @see Num
 */
public class Datetime extends Column
{
	/**
	 *	设置pojo类对应的属性.
	 *
	 *	@param property pojo类对应的属性
	 *  @return 当前对象
	 */
	public Datetime property(String property)
	{
		this.property = property;
		return this;
	}

	/**
	 *	对当前字段进行“等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr eq(String value)
	{
		boolean isnull = value==null || value.length()==0;
		if(isnull) return new BoolExpr(this.getAliasWithName()+" is null", isnull);
		else return new BoolExpr(this.getAliasWithName()+"='"+value+"'", isnull);
	}
	
	/**
	 *	对当前字段进行“等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr eq(Number value)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr(this.getAliasWithName()+" is null", isnull);
		else return new BoolExpr(this.getAliasWithName()+"="+value.longValue(), isnull);
	}

	/**
	 *	对当前字段进行“等于”判断.
	 *
	 *	@param value 被比较的值
	 *	@param format 日期格式
	 *  @return BoolExpr对象
	 */
	public BoolExpr eq(Date value, SimpleDateFormat format)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr(this.getAliasWithName()+" is null", isnull);
		else return new BoolExpr(this.getAliasWithName()+"='"+format.format(value)+"'", isnull);
	}

	/**
	 *	对当前字段进行“不等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr notEq(String value)
	{
		boolean isnull = value==null || value.length()==0;
		if(isnull) return new BoolExpr(this.getAliasWithName()+" is not null", isnull);
		else return new BoolExpr(this.getAliasWithName()+"<>'"+value+"'", isnull);
	}
	
	/**
	 *	对当前字段进行“不等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr notEq(Number value)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr(this.getAliasWithName()+" is not null", isnull);
		else return new BoolExpr(this.getAliasWithName()+"<>"+value.longValue(), isnull);
	}

	/**
	 *	对当前字段进行“不等于”判断.
	 *
	 *	@param value 被比较的值
	 *	@param format 日期格式
	 *  @return BoolExpr对象
	 */
	public BoolExpr notEq(Date value, SimpleDateFormat format)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr(this.getAliasWithName()+" is not null", isnull);
		else return new BoolExpr(this.getAliasWithName()+"<>'"+format.format(value)+"'", isnull);
	}
	
	/**
	 *	对当前字段进行“大于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr gt(String value)
	{
		boolean isnull = value==null || value.length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+">'"+value+"'", isnull);
	}
	
	/**
	 *	对当前字段进行“大于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr gt(Number value)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+">"+value.longValue(), isnull);
	}

	/**
	 *	对当前字段进行“大于”判断.
	 *
	 *	@param value 被比较的值
	 *	@param format 日期格式
	 *  @return BoolExpr对象
	 */
	public BoolExpr gt(Date value, SimpleDateFormat format)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+">'"+format.format(value)+"'", isnull);
	}

	/**
	 *	对当前字段进行“大于等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr ge(String value)
	{
		boolean isnull = value==null || value.length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+">='"+value+"'", isnull);
	}
	
	/**
	 *	对当前字段进行“大于等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr ge(Number value)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+">="+value.longValue(), isnull);
	}

	/**
	 *	对当前字段进行“大于等于”判断.
	 *
	 *	@param value 被比较的值
	 *	@param format 日期格式
	 *  @return BoolExpr对象
	 */
	public BoolExpr ge(Date value, SimpleDateFormat format)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+">='"+format.format(value)+"'", isnull);
	}

	/**
	 *	对当前字段进行“小于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr lt(String value)
	{
		boolean isnull = value==null || value.length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+"<'"+value+"'", isnull);
	}
	
	/**
	 *	对当前字段进行“小于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr lt(Number value)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+"<"+value.longValue(), isnull);
	}

	/**
	 *	对当前字段进行“小于判断.
	 *
	 *	@param value 被比较的值
	 *	@param format 日期格式
	 *  @return BoolExpr对象
	 */
	public BoolExpr lt(Date value, SimpleDateFormat format)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+"<'"+format.format(value)+"'", isnull);
	}

	/**
	 *	对当前字段进行“小于等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr le(String value)
	{
		boolean isnull = value==null || value.length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+"<='"+value+"'", isnull);
	}
	
	/**
	 *	对当前字段进行“小于等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr le(Number value)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+"<="+value.longValue(), isnull);
	}

	/**
	 *	对当前字段进行“小于等于”判断.
	 *
	 *	@param value 被比较的值
	 *	@param format 日期格式
	 *  @return BoolExpr对象
	 */
	public BoolExpr le(Date value, SimpleDateFormat format)
	{
		boolean isnull = value==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+"<='"+format.format(value)+"'", isnull);
	}

	/**
	 *	对当前字段进行“is null”判断.
	 *
	 *  @return BoolExpr对象
	 */
	public BoolExpr isNull()
	{
		return new BoolExpr(this.getString()+" is null");
	}

	/**
	 *	对当前字段进行“is not null”判断.
	 *
	 *  @return BoolExpr对象
	 */
	public BoolExpr notNull()
	{
		return new BoolExpr(this.getString()+" is not null");
	}
}