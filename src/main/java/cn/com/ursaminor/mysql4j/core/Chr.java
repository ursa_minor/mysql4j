package cn.com.ursaminor.mysql4j.core;

import java.util.Collection;

/**
 * 该类定义一个字符型字段.
 *  
 * @author 小熊
 * @version 1.0
 * @see Column
 * @see Num
 * @see Datetime
 */
public class Chr extends Column
{
	/**
	 *	设置pojo类对应的属性.
	 *
	 *	@param property pojo类对应的属性
	 *  @return 当前对象
	 */
	public Chr property(String property)
	{
		this.property = property;
		return this;
	}

	/**
	 *	当前字段作为主键.
	 *
	 *  @return 当前对象
	 */
	public Chr asPrimaryKey()
	{
		this.pk = true;
		return this;
	}

	/**
	 *	对当前字段进行“等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr eq(String value)
	{
		boolean isnull = value==null || value.length()==0;
		if(value==null) return new BoolExpr(this.getAliasWithName()+" is null", isnull);
		else return new BoolExpr(this.getAliasWithName()+"='"+value+"'", isnull);
	}

	/**
	 *	对当前字段进行“等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr eq(StringChip value)
	{
		boolean isnull = value==null || value.getString()==null || value.getString().length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+"="+value.getString(), isnull);
	}

	/**
	 *	对当前字段进行“不等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr notEq(String value)
	{
		boolean isnull = value==null || value.length()==0;
		if(value==null) return new BoolExpr(this.getAliasWithName()+" is not null", isnull);
		else return new BoolExpr(this.getAliasWithName()+"<>'"+value+"'", isnull);
	}

	/**
	 *	对当前字段进行“不等于”判断.
	 *
	 *	@param value 被比较的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr notEq(StringChip value)
	{
		boolean isnull = value==null || value.getString()==null || value.getString().length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+"<>"+value.getString(), isnull);
	}

	/**
	 *	对当前字段进行“like”判断.
	 *
	 *	@param value 被匹配的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr like(String value)
	{
		boolean isnull = value==null || value.length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" like '"+value+"'", isnull);
	}

	/**
	 *	对当前字段进行“not like”判断.
	 *
	 *	@param value 被匹配的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr notLike(String value)
	{
		boolean isnull = value==null || value.length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" not like '"+value+"'", isnull);
	}

	/**
	 *	对当前字段进行“like '%value%'”判断.
	 *
	 *	@param value 被匹配的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr contains(String value)
	{
		boolean isnull = value==null || value.length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" like '%"+value+"%'", isnull);
	}

	/**
	 *	对当前字段进行“not like '%value%'”判断.
	 *
	 *	@param value 被匹配的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr notContains(String value)
	{
		boolean isnull = value==null || value.length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" not like '%"+value+"%'", isnull);
	}

	/**
	 *	对当前字段进行“like 'value%'”判断..
	 *
	 *	@param value 被匹配的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr startsWith(String value)
	{
		boolean isnull = value==null || value.length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" like '"+value+"%'", isnull);
	}

	/**
	 *	对当前字段进行“like '%value'”判断.
	 *
	 *	@param value 被匹配的值
	 *  @return BoolExpr对象
	 */
	public BoolExpr endsWith(String value)
	{
		boolean isnull = value==null || value.length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" like '%"+value+"'", isnull);
	}

	/**
	 *	对当前字段进行“in”判断.
	 *
	 *	@param values 被匹配的值数组
	 *  @return BoolExpr对象
	 */
	public BoolExpr in(String... values)
	{
		String strArray = getString(values);
		boolean isnull = strArray==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" in ("+strArray+")", isnull);
	}

	/**
	 *	对当前字段进行“in”判断.
	 *
	 *	@param values 被匹配的值集合
	 *  @return BoolExpr对象
	 */
	public BoolExpr in(Collection<String> values)
	{
		String strArray = getString(values);
		boolean isnull = strArray==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" in ("+strArray+")", isnull);
	}

	/**
	 *	对当前字段进行“in”判断.
	 *
	 *	@param query 被匹配的子查询
	 *  @return BoolExpr对象
	 */
	public BoolExpr in(Query query)
	{
		String sub = query==null?null:query.getSql();
		boolean isnull = query==null || sub==null || sub.length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" in ("+sub+")", isnull);
	}

	/**
	 *	对当前字段进行“not in”判断.
	 *
	 *	@param values 被匹配的值数组
	 *  @return BoolExpr对象
	 */
	public BoolExpr notIn(String... values)
	{
		String strArray = getString(values);
		boolean isnull = strArray==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" not in ("+strArray+")", isnull);
	}

	/**
	 *	对当前字段进行“not in”判断.
	 *
	 *	@param values 被匹配的值集合
	 *  @return BoolExpr对象
	 */
	public BoolExpr notIn(Collection<String> values)
	{
		String strArray = getString(values);
		boolean isnull = strArray==null;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" not in ("+strArray+")", isnull);
	}

	/**
	 *	对当前字段进行“not in”判断.
	 *
	 *	@param query 被匹配的子查询
	 *  @return BoolExpr对象
	 */
	public BoolExpr notIn(Query query)
	{
		String sub = query==null?null:query.getSql();
		boolean isnull = query==null || sub==null || sub.length()==0;
		if(isnull) return new BoolExpr("0=1", isnull);
		else return new BoolExpr(this.getAliasWithName()+" not in ("+sub+")", isnull);
	}

	private String getString(String... values)
	{
		if(values==null) return null;
		String s = null;
		for(String v : values)
		{
			if(v==null || v.length()==0) continue;
			if(s==null) s = "'"+v+"'";
			else s += ",'"+v+"'";
		}
		return s;
	}

	private String getString(Collection<String> values)
	{
		if(values==null) return null;
		String s = null;
		for(String v : values)
		{
			if(v==null || v.length()==0) continue;
			if(s==null) s = "'"+v+"'";
			else s += ",'"+v+"'";
		}
		return s;
	}

	/**
	 *	对当前字段进行“is null”判断.
	 *
	 *  @return BoolExpr对象
	 */
	public BoolExpr isNull()
	{
		return new BoolExpr(this.getString()+" is null");
	}

	/**
	 *	对当前字段进行“is not null”判断.
	 *
	 *  @return BoolExpr对象
	 */
	public BoolExpr notNull()
	{
		return new BoolExpr(this.getString()+" is not null");
	}
}