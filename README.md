### Mysql4J

 
 **介绍** 

Mysql4J是一款用于访问mysql数据库的ORM组件。通过它可以方便地构建sql语句，并且可在编译期进行sql语法检查，还可以把得到的数据结果转换成List、Map、Set等形式。简单易用，独辟蹊径，是提高代码编写效率的好工具。

 **特点** 

1. 底层基于Spring的JdbcTemplate。
    Mysql4J底层通过Spring的JdbcTemplate对象访问数据库，该JdbcTemplate对象需要事先配置好并保证可用，然后设置到Mysql4J
中。
2. 编译期纠错。
    Mysql4J可在编译期检查出大部分的sql语法错误，相比于在运行时才发现sql错误，对于快速开发，有很大帮助。
由于用java模拟了sql语句，所以sql的语法，对应到了java语法，这样当sql拼写不正确时（逻辑错误除外），通常也是一个java语法错误，如果使用IDE的代码提示工具，则基本的匹配和代码构建是自
动提示的。
3. 数据结果，封装友好。
    Mysql4J对查询得到的数据，可进行相对灵活的后处理。可以是pojo对象的List集合，也可以是Map集合。和JdbcTemplate不同，JdbcTemplate的getMap方法，仅返回Map表示的一条数据记录，Mysql4J则把多行查询结果，放在Map中，Map的key值，可以由程序员指定，通常指定成表中某个不重复的字段。还可以方便的直接得到单个值，或者某一列值的List列表。
如果不想定义pojo对象，可以直接使用Row对象封装行记录，Row实现了Map接口，提供了getString、getInteger、getDate等方便的数据提取方法。
4. api简单易懂，上手迅速。
    Mysql4J的api非常简单，几乎不用阅读文档，仅根据java IDE的代码工具提示，就可以构建完整的
程序代码。
5. sql即用即写，逻辑集中不分散。
    sql语句中，通常包含着关键的业务逻辑。Mysql4J可保证在一个java方法中，业务逻辑的完整性，sql语句不会被写到程序文件之外。打开一个类，逻辑全部在其内部展现，相对于把sql写到xml中，代码更清晰易读。

 **安装教程** 

下载后，项目中引入mysql4j_1.0.1.jar即可。

 **使用说明** 

Mysql4J使用说明.pdf

 **API文档**

mysql4j_1.0.1_api_doc.zip
